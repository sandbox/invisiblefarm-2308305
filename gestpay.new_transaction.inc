<?php

/**
 * @file
 * Provides the Gestpay Easynolo (Banca Sella) Italian payment standalone module for Drupal
 *
 * Author: Gianfranco Andreoli - Invisiblefarm S.r.l.
 * URL: http://www.invisiblefarm.it
 */

/**
 * New transaction callback
 */
function new_transaction_form($form, $form_state) {
	
	
	drupal_add_js(array('gestpay_images' =>  drupal_get_path('module', 'gestpay').'/images'), 'setting');
	drupal_add_js(drupal_get_path('module', 'gestpay') . '/js/loadingimage.js');
	
	// error if shop login is not set
	$shopLogin = variable_get('gestpay_shopLogin', NULL);
	if($shopLogin == NULL){
		drupal_set_message(t('Error: Shop Login is not set. Contact site admin.'), 'error');
	}
	
	// shop login
	$form['amount'] = array(
		'#type' => 'textfield',
		'#title' => t('Amount'),
		'#default_value' => 0.00,
		'#description' => t("Amount to pay."),
		'#required' => TRUE,
		);
	
	// shop login
	$form['note'] = array(
		'#type' => 'textarea',
		'#title' => t('Note'),
		'#description' => t("Payment note."),
		'#required' => FALSE,
		);

	$form['submit'] = array(
		'#type' => 'submit',
		'#description' => t('Go to payment platform.'),
		'#value' => t('Go to payment platform.'),
	);
	
	return $form;    
}

/**
 * Form validate
 * @param type $form
 * @param type $form_state 
 */
function new_transaction_form_validate($form, $form_state) {
	
	$amount = $form['amount']['#value'];
	$amount = str_replace(",", ".", $amount);
	
	//must be numeric
	if (!is_numeric($amount)){
		form_error($form['amount'], t('Amount is not numeric.'));
	}
	//must be in number format 2 decimal
	elseif(number_format($amount, 2, '.', '') != $amount){
		form_error($form['amount'], t('Amount is not money format').' ('.number_format($amount, 2, '.', '').').');
	}
	//must be positive
	elseif($amount <= 0){
		form_error($form['amount'], t('Amount must be positive.'));
	}
	
}

/**
 * hook of submit
 * @global type $user
 * @param type $form
 * @param type $form_state 
 */
function new_transaction_form_submit($form, $form_state) {
	
	global $user;

	$amount = $form['amount']['#value'];
	$amount = str_replace(",", ".", $amount);
	
	$note = $form['note']['#value'];

	//get variable
	$shopLogin = variable_get('gestpay_shopLogin', NULL);
	$server_type = variable_get('gestpay_serverType', 'Test');
	$uicCode = variable_get('gestpay_uic', 242);
	
	$insert = array(
		'uid' => $user->uid,
		'payment_note' => $note,
		'insert_time' => date('Y-m-d H:i:s'),
	);	
	
	$shop_transaction_id = db_insert('gestpay_transaction')
		->fields($insert)
		->execute();
	
	// Call Gestpay WSCryptDecrypt WebService using SOAP
	$client = new SoapClient(getGestpayWSDLServerUrl($server_type));
	
	$params = new stdClass();
	
	$params->shopLogin = $shopLogin;
	$params->uicCode = $uicCode;
	$params->amount = $amount;
	$params->shopTransactionId = $shop_transaction_id;
	$params->buyerName = $user->name;
	$params->buyerEmail = $user->mail;

	// Encrypt data
	$objectresult = $client->Encrypt($params);
	
	// Get encrypted data
	$simpleresult = $objectresult->EncryptResult;
	
	$xml = simplexml_load_string($simpleresult->any);
	
	
	if ($xml->TransactionResult == "OK" ) {
		
		$update = array(
			'crypted_transaction' => $xml->CryptDecryptString,
		);
		
		db_update('gestpay_transaction')
				->fields($update)
				->condition('shop_transaction_id',$shop_transaction_id,'=')
				->execute();
		
		 // Generate Gestpay parameters
	    $a = $shopLogin;
	    $b = $xml->CryptDecryptString;
		
		$link= getGestpayPaymentServerUrl($server_type) . "?a=" . $a . "&b=" . $b;
		
		header('Location: '.$link);
		
		exit;

	} else {
		drupal_set_message('Error code '.$xml->ErrorCode.': '.$xml->ErrorDescription, 'error');
	}
}