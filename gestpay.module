<?php

/**
 * @file
 * Provides the Gestpay Easynolo (Banca Sella) Italian payment standalone module for Drupal
 *
 * Author: Gianfranco Andreoli - Invisiblefarm S.r.l.
 * URL: http://www.invisiblefarm.it
 */

/*
 * load utility file
 */
module_load_include('inc', 'gestpay', 'lib/gestpay.utility');


/**
 * Implementation of hook_permission()
 */
function gestpay_permission() {
    
    $items = array();
    
    $items['gestpay module'] = array(
        'title' => t('Use Gestpay module'),
        'description' => t('Allow users to use GestPay module'),
        );
	
	$items['administer gestpay settings'] = array(
        'title' => t('Administer Gestpay module'),
        'description' => t('Allow users to administer GestPay module'),
        );
    
    return $items;
}


/**
 * Implements hook_menu().
 */
function gestpay_menu() {
	
	// payment area in config panel
	$items['admin/config/payment'] = array(
		'title' => 'Payment',
		'description' => 'Configure payment method.',
		'position' => 'left',
		'page callback' => 'system_admin_menu_block_page',
		'access arguments' => array('access administration pages'),
		'file' => 'system.admin.inc',
		'file path' => drupal_get_path('module', 'system'),
		);
	
	//admin page
	$items['admin/config/payment/gestpay'] = array(
		'title' => 'Gestpay module settings',
		'description' => 'Set variables to work with GestPay payment',
		'file' => 'gestpay.admin.inc',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('gestpay_admin'),
		'access arguments' => array('administer gestpay settings'),
		'type' => MENU_NORMAL_ITEM,
		);
    
	//new_transaction
	$items['gestpay/new_transaction'] = array(
		'title' => 'New transaction',
		'file' => 'gestpay.new_transaction.inc',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('new_transaction_form'),
		'access callback' => 'user_access',
		'access arguments' => array('gestpay module'),
		'type' => MENU_NORMAL_ITEM,
		);
	
	//transaction server to server outcome
	$items['gestpay/transaction_server_to_server_outcome'] = array(
		'title' => '',
		'file' => 'gestpay.transaction_server_to_server_outcome.inc',
		'page callback' => 'page_transaction_server_to_server_outcome',
		'access callback' => TRUE,
		'type' => MENU_CALLBACK,
		);
	
	// positive outcome
	$items['gestpay/positive_outcome'] = array(
		'title' => 'Positive transaction outcome',
		'file' => 'gestpay.outcome.inc',
		'page callback' => 'page_positive_outcome',
		'access callback' => 'user_access',
		'access arguments' => array('gestpay module'),
		'type' => MENU_CALLBACK,
		);
	
	// negative outcome
	$items['gestpay/negative_outcome'] = array(
		'title' => 'Negative transaction outcome',
		'file' => 'gestpay.outcome.inc',
		'page callback' => 'page_negative_outcome',
		'access callback' => 'user_access',
		'access arguments' => array('gestpay module'),
		'type' => MENU_CALLBACK,
		);

    return $items;
}

/**
 * Implements hook_help().
 */
function gestpay_help($path, $arg) {
	
	
	global $base_url;
	
	switch ($path) {
		
		case 'admin/help#gestpay':
			
			$output = '';
			$output .= '<h3>' . t('About') . '</h3>';
			$output .= '<p>' . t('This module provides the GestPay Easynolo (Banca Sella) Italian payment stand-alone module for Drupal.') . '</p>';
			$output .= '<h3>' . t('Uses') . '</h3>';
			$output .= '<dl>';
			$output .= '<dt>' . t('Module configuration') . '</dt>';
			$output .= '<dd>' . t('Set GestPay Easynolo configuration in <a href="@gestpay_module_settings">GestPay module settings</a>. Pay attention to ShopLogin field.', array('@gestpay_module_settings' => url('admin/config/payment/gestpay'))) . '</dd>';
			$output .= '<dt>' . t('Set transaction page') . '</dt>';
			$output .= '<dd>' . t('Create a link in your web site to the <a href="@transaction_page">transaction page</a>.', array('@transaction_page' => url('gestpay/new_transaction'))) . '</dd>';
			$output .= '<dt>' . t('Settings in GestPay backoffice') . '</dt>';
			$output .= '<dd>' . t('Set on your GestPay backoffice page the positive URL response as <a href="@positive_response_url">@positive_response_url</a>.', array('@positive_response_url' =>  $base_url.'/gestpay/positive_outcome')) . '</dd>';
			$output .= '<dd>' . t('Set on your GestPay backoffice page the negative URL response as <a href="@negative_response_url">@negative_response_url</a>.', array('@negative_response_url' => $base_url.'/gestpay/negative_outcome')) . '</dd>';
			$output .= '<dd>' . t('Set on your GestPay backoffice page the server to server URL response as <a href="@sts_response_url">@sts_response_url</a>.', array('@sts_response_url' => $base_url.'/gestpay/transaction_server_to_server_outcome')) . '</dd>';			
			$output .= '<dd>' . t('Set Buyer Email and Buyer Name as a parameter in GestPay backoffice.') . '</dd>';
			$output .= '<dt>' . t('Set permission') . '</dt>';
			$output .= '<dd>' . t('Enable "Use GestPay module" permission to role you want have right to pay, in  <a href="@permission">permission page</a>.', array('@permission' => url('admin/people/permissions'))) . '</dd>';
			$output .= '</dl>';
			return $output;
			break;
		
	}
}