(function($) {
	
	$(document).ready(function() {
		
		$("body").prepend('<div id="PleaseWait" style="z-index:1002;display:none;position:absolute;left:50%;top:50%;"><img src="' + Drupal.settings.gestpay_images + '/loader.gif" style="margin-left:-110px;margin-top:-9px;" /></div>');
		$("body").prepend('<div id="overlay" class="ui-widget-overlay" style="z-index:1001;display:none;"></div>');
		
		$('#new-transaction-form').submit(function() {
			$("#overlay, #PleaseWait").show();
		});
	});
})(jQuery);