<?php

/**
 * @file
 * Provides the Gestpay Easynolo (Banca Sella) Italian payment standalone module for Drupal
 *
 * Author: Gianfranco Andreoli - Invisiblefarm S.r.l.
 * URL: http://www.invisiblefarm.it
 */


function gestpay_admin() {
	
	$form = array();
	
	// shop login
	$form['gestpay_shopLogin'] = array(
		'#type' => 'textfield',
		'#title' => t('Shop login'),
		'#default_value' => variable_get('gestpay_shopLogin', NULL),
		'#description' => t("Login shop for GestPay service."),
		'#required' => TRUE,
		);
	
	$uic_options = array(
		1 => 'USD',
		2 => 'GBP',
		3 => 'CHF',
		//7 => 'DKK',
		//8 => 'NOK',
		//9 => 'SEK',
		12 => 'CAD',
		//18 => 'ITL',
		71 => 'JPY',
		//103 => 'HKD',
		//234 => 'BRL',
		242 => 'EUR',
	);
	
	asort($uic_options);
	
	
	// server type
	$form['gestpay_uic'] = array(
		'#type' => 'select',
		'#title' => t('Currency'),
		'#default_value' => variable_get('gestpay_uic', 242),
		'#options' => $uic_options,
		'#description' => t("Default currency for GestPay payment."),
		'#required' => TRUE,
		);
	
	
	// server type
	$form['gestpay_serverType'] = array(
		'#type' => 'radios',
		'#title' => t('Server type'),
		'#default_value' => variable_get('gestpay_serverType', 'Test'),
		'#options' => array( 'Test' => 'Test', 'Live' => 'Live'),
		'#description' => t("Server type for GestPay service."),
		'#required' => TRUE,
		);
	
	// WSDL server test
	$form['gestpay_wsdlServerTest'] = array(
		'#type' => 'textfield',
		'#title' => t('GestPay WSDL server test'),
		'#default_value' => variable_get('gestpay_wsdlServerTest', 'https://testecomm.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL'),
		'#description' => t("GestPay WSDL server test, default: https://testecomm.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL."),
		'#required' => TRUE,
		);
	
	// WSDL server live
	$form['gestpay_wsdlServerLive'] = array(
		'#type' => 'textfield',
		'#title' => t('GestPay WSDL server live'),
		'#default_value' => variable_get('gestpay_wsdlServerLive', 'https://ecomms2s.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL'),
		'#description' => t("GestPay WSDL server live, default: https://ecomms2s.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL."),
		'#required' => TRUE,
		);
	
	// Payment server test
	$form['gestpay_paymentServerTest'] = array(
		'#type' => 'textfield',
		'#title' => t('GestPay payment server test'),
		'#default_value' => variable_get('gestpay_paymentServerTest', 'https://testecomm.sella.it/pagam/pagam.aspx'),
		'#description' => t("GestPay payment server test, default: https://testecomm.sella.it/pagam/pagam.aspx."),
		'#required' => TRUE,
		);
	
	// Payment server live
	$form['gestpay_paymentServerLive'] = array(
		'#type' => 'textfield',
		'#title' => t('GestPay payment server live'),
		'#default_value' => variable_get('gestpay_paymentServerLive', 'https://ecomm.sella.it/pagam/pagam.aspx'),
		'#description' => t("GestPay payment server live, default: https://ecomm.sella.it/pagam/pagam.aspx."),
		'#required' => TRUE,
		);
	
	
	
	return system_settings_form($form);
}