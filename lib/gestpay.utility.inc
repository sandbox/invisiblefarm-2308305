<?php

/**
 * @file
 * Provides the Gestpay Easynolo (Banca Sella) Italian payment standalone module for Drupal
 *
 * Utility file
 * 
 * Author: Gianfranco Andreoli - Invisiblefarm S.r.l.
 * URL: http://www.invisiblefarm.it
 */



/**
 * Returns the URL with the WSCryptDecrypt WebService
 */
function getGestpayWSDLServerUrl($server) {
	
	$wsdl_server_test = variable_get('gestpay_wsdlServerTest', 'https://testecomm.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL');
    $wsdl_server_live = variable_get('gestpay_wsdlServerLive', 'https://ecomms2s.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL');
	
	
    switch ($server) {
        case 'test':
            return $wsdl_server_test;
            break;
        case 'live':
            return $wsdl_server_live;
            break;
        default :
            //defaul test server
            return $wsdl_server_test;
            break;
        
            }
}


/**
 * Returns the URL with the WSCryptDecrypt WebService
 */
function getGestpayPaymentServerUrl($server) {
	
	$payment_server_test = variable_get('gestpay_paymentServerTest', 'https://testecomm.sella.it/pagam/pagam.aspx');
	$payment_server_live = variable_get('gestpay_paymentServerLive', 'https://ecomm.sella.it/pagam/pagam.aspx');
    
    switch ($server) {
        case 'test':
            return $payment_server_test;
            break;
        case 'live':
            return 'https://ecomm.sella.it/pagam/pagam.aspx';
            break;
        default :
            //defaul test server
            return $payment_server_test;
            break;
        
            }
}