<?php

/**
 * @file
 * Provides the Gestpay Easynolo (Banca Sella) Italian payment standalone module for Drupal
 *
 * Author: Gianfranco Andreoli - Invisiblefarm S.r.l.
 * URL: http://www.invisiblefarm.it
 */

/**
 * Page
 */
function page_positive_outcome() {
	
	$server_type = variable_get('gestpay_serverType', 'Test');
	
	// Call Gestpay WSCryptDecrypt WebService using SOAP
	$client = new SoapClient(getGestpayWSDLServerUrl($server_type));

	$params = new stdClass();
	
	$params->shopLogin = $_GET["a"];
	$params->CryptedString = $_GET["b"];

	$objectresult = $client->Decrypt($params);
	$simpleresult = $objectresult->DecryptResult;

	$xml = simplexml_load_string($simpleresult->any);
	
	$html='';
	
	$update = array(
		'crypted_response' => $params->CryptedString,
		'transaction_result' => $xml->TransactionResult,
		'shop_transaction_id' => $xml->ShopTransactionID,
		'bank_transaction_id' => $xml->BankTransactionID,
		'authorization_code' => $xml->AuthorizationCode,
		'currency' => $xml->Currency,
		'amount' => $xml->Amount,
		'buyer_name' => $xml->Buyer->BuyerName,
		'buyer_email' => $xml->Buyer->BuyerEmail,
		'error_code' => $xml->ErrorCode,
		'error_description' => $xml->ErrorDescription,
		'insert_time' => date('Y-m-d H:i:s'),
	);
	
	db_update('gestpay_transaction')
				->fields($update)
				->condition('shop_transaction_id',$xml->ShopTransactionID,'=')
				->execute();
	
	$html .= $xml->ErrorDescription;
	$html .= '<br>';
	$html .= l(t('New transaction.'), 'gestpay/new_transaction');
	
	return $html;
}

/**
 * Page
 */
function page_negative_outcome() {
	
	$server_type = variable_get('gestpay_serverType', 'Test');
	
	// Call Gestpay WSCryptDecrypt WebService using SOAP
	$client = new SoapClient(getGestpayWSDLServerUrl($server_type));

	$params = new stdClass();
	
	$params->shopLogin = $_GET["a"];
	$params->CryptedString = $_GET["b"];

	$objectresult = $client->Decrypt($params);
	$simpleresult = $objectresult->DecryptResult;

	$xml = simplexml_load_string($simpleresult->any);
	
	$html='';
	
	$update = array(
		'crypted_response' => $params->CryptedString,
		'transaction_result' => $xml->TransactionResult,
		'shop_transaction_id' => $xml->ShopTransactionID,
		'bank_transaction_id' => $xml->BankTransactionID,
		'authorization_code' => $xml->AuthorizationCode,
		'currency' => $xml->Currency,
		'amount' => $xml->Amount,
		'buyer_name' => $xml->Buyer->BuyerName,
		'buyer_email' => $xml->Buyer->BuyerEmail,
		'error_code' => $xml->ErrorCode,
		'error_description' => $xml->ErrorDescription,
		'insert_time' => date('Y-m-d H:i:s'),
	);
	
	db_update('gestpay_transaction')
				->fields($update)
				->condition('shop_transaction_id',$xml->ShopTransactionID,'=')
				->execute();
	
	$html .= $xml->ErrorDescription;
	$html .= '<br>';
	$html .= l(t('New transaction.'), 'gestpay/new_transaction');
	
	return $html;
}